import QtQuick 2.15
import QtMultimedia

Item {

    property int side: 5
    property string myColor: "red"

    Rectangle
    {
        width: side
        height: side
        color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
        border.color: "black"
        border.width: 5
        radius: 10

        MouseArea
        {
            anchors.fill: parent
            onClicked: { parent.color = myColor }
        }

    }

}
