import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts
import QtMultimedia

Window
{
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    property int side: 100

    RowLayout
    {

        ColumnLayout
        {
            spacing: 2

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "AAAAAGGGGHHHHHH"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_1
                    source: "qrc:/sound/AAAAGGHHHH.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_1.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Alert Tone"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_2
                    source: "qrc:/sound/alertTone.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_2.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Bells"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_3
                    source: "qrc:/sound/bells.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_3.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Bright Charm"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_4
                    source: "qrc:/sound/brightCharm.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_4.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }
        }

        ColumnLayout
        {
            spacing: 2

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "EA"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_5
                    source: "qrc:/sound/ea.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_5.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Electric Power"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_6
                    source: "qrc:/sound/electricPower.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_6.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Fog Horn"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_7
                    source: "qrc:/sound/fogHorn.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_7.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Game Cube"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_8
                    source: "qrc:/sound/gameCube.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_8.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }
        }

        ColumnLayout
        {
            spacing: 2

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Goofy"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_9
                    source: "qrc:/sound/goffy.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_9.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Gulp"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_10
                    source: "qrc:/sound/gulp.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_10.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Heigh Hat"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_11
                    source: "qrc:/sound/heightHat.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_11.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Jiggle"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_12
                    source: "qrc:/sound/jiggle.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_12.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }
        }

        ColumnLayout
        {
            spacing: 2

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Laser"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_13
                    source: "qrc:/sound/laser.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_13.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10

                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Panther Crying"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_14
                    source: "qrc:/sound/pantherCrying.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_14.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "Plucked Nylon"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_15
                    source: "qrc:/sound/pluckedNylon.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_15.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }

            Rectangle
            {
                width: side
                height: side
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random());
                border.color: "black"
                border.width: 5
                radius: 10
                Text
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "What the dog doing"
                    font.pointSize: 5
                    color: "black"
                }

                SoundEffect
                {
                    id: playSound_16
                    source: "qrc:/sound/whatTheDogDoing.wav"
                }

                MouseArea
                {
                    anchors.fill: parent
                    onPressed: { parent.color =  "black"; playSound_16.play();}
                    onReleased: parent.color = Qt.rgba(Math.random(),Math.random(),Math.random(),Math.random())
                }
            }
        }
    }
}
